package interactive.cide.com.kuriocompagnon.ui.association;

import android.net.nsd.NsdServiceInfo;

public interface ServiceListener {
    void onServiceReady(NsdServiceInfo serviceInfo);
}
