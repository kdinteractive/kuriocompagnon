package interactive.cide.com.kuriocompagnon.ui.timecontrol;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import interactive.cide.com.kuriocompagnon.R;

public class TCExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private String[] listDataHeader;
    private HashMap<String, String[]> _listDataChild;
    SharedPreferences sharedPreferences;
    int timeToStart, timeToEnd;
    ImageButton ibMoreStartTimeAuthorized, ibLessStartTimeAuthorized, ibMoreEndTimeAuthorized, ibLessEndTimeAuthorized,ibTime;
    TextView tvStartDailyTime, tvEndDailyTime, tvStartTimeAuthorized, tvEndTimeAuthorized;
    SeekBar seekBar;


    public TCExpandableListAdapter(Context context, String[] listDataHeader,
                                   HashMap<String, String[]> listChildData, ExpandableListView expListView) {
        this._context = context;
        this.listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        sharedPreferences = _context.getSharedPreferences("CHILDINFO", 0);
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this.listDataHeader[groupPosition])[childPosititon];
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, final ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_time_control, null);
        }

        //Initialization

        ibMoreStartTimeAuthorized = (ImageButton) convertView.findViewById(R.id.ibMoreStartTimeAuthorized);
        ibLessStartTimeAuthorized = (ImageButton) convertView.findViewById(R.id.ibLessStartTimeAuthorized);
        ibMoreEndTimeAuthorized = (ImageButton) convertView.findViewById(R.id.ibMoreEndTimeAuthorized);
        ibLessEndTimeAuthorized = (ImageButton) convertView.findViewById(R.id.ibLessEndTimeAuthorized);
        tvStartDailyTime = (TextView) convertView.findViewById(R.id.tvStartDailyTime);
        tvEndDailyTime = (TextView) convertView.findViewById(R.id.tvEndDailyTime);
        tvStartTimeAuthorized = (TextView) convertView.findViewById(R.id.tvStartAuthorizedHours);
        tvEndTimeAuthorized = (TextView) convertView.findViewById(R.id.tvEndAuthorizedHours);
        seekBar = convertView.findViewById(R.id.rangeBar);
        ibTime = (ImageButton) convertView.findViewById(R.id.ibTime);
        ibTime.setImageDrawable(convertView.getContext().getResources().getDrawable(R.drawable.ic_access_time_black_24dp));
        seekBar.setFocusable(false);
        tvStartTimeAuthorized.setText(_listDataChild.get(listDataHeader[groupPosition])[0]);
        tvEndTimeAuthorized.setText(_listDataChild.get(listDataHeader[groupPosition])[1]);
        timeToStart = Integer.parseInt(_listDataChild.get(listDataHeader[groupPosition])[0].split(":")[0]);
        timeToEnd = Integer.parseInt(_listDataChild.get(listDataHeader[groupPosition])[1].split(":")[0]);
        int hoursDaily = Integer.parseInt(_listDataChild.get(listDataHeader[groupPosition])[2].split(":")[0]);
        int minutesDaily = Integer.parseInt(_listDataChild.get(listDataHeader[groupPosition])[2].split(":")[1]);
        int progressToSet = hoursDaily == 0 && minutesDaily == 30? 1: hoursDaily*2;
        seekBar.setMax((timeToEnd - timeToStart) * 2);
        seekBar.setProgress(progressToSet);
        tvStartDailyTime.setText(_listDataChild.get(listDataHeader[groupPosition])[2]);
        tvEndDailyTime.setText((timeToEnd - timeToStart) + ":00");


        //Behavior Buttons + -

        View.OnClickListener listenerMoreOrLess = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeToStart = Integer.parseInt(_listDataChild.get(listDataHeader[groupPosition])[0].split(":")[0]);
                timeToEnd = Integer.parseInt(_listDataChild.get(listDataHeader[groupPosition])[1].split(":")[0]);
                switch(v.getId()) {
                    case R.id.ibMoreStartTimeAuthorized:
                        if (timeToStart > 5 && timeToStart < 23 && timeToStart + 1 < timeToEnd) {
                            timeToStart++;
                            tvStartTimeAuthorized.setText(timeToStart + ":00");
                            _listDataChild.get(listDataHeader[groupPosition])[0] = timeToStart + ":00";
                        } else if (timeToStart + 1 >= timeToEnd) {
                            //DO NOTHING
                        } else {
                            Toast.makeText(parent.getContext(), "Authorized time has to be between 6:00 - 23:00", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.ibLessStartTimeAuthorized:
                        if (timeToStart > 6 && timeToStart < 24 && timeToStart < timeToEnd) {
                            timeToStart--;
                            tvStartTimeAuthorized.setText(timeToStart + ":00");
                            _listDataChild.get(listDataHeader[groupPosition])[0] = timeToStart + ":00";
                        } else if (timeToStart >= timeToEnd) {
                            //DO NOTHING
                        } else {
                            Toast.makeText(parent.getContext(), "Authorized time has to be between 6:00 - 23:00", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.ibMoreEndTimeAuthorized:
                        if (timeToEnd > 5 && timeToEnd < 23 && timeToStart < timeToEnd) {
                            timeToEnd++;
                            tvEndTimeAuthorized.setText(timeToEnd + ":00");
                            _listDataChild.get(listDataHeader[groupPosition])[1] = timeToEnd + ":00";
                        } else if (timeToStart >= timeToEnd) {
                            //DO NOTHING
                        } else {
                            Toast.makeText(parent.getContext(), "Authorized time has to be between 6:00 - 23:00", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.ibLessEndTimeAuthorized:
                        if (timeToEnd > 6 && timeToEnd < 24 && timeToStart < timeToEnd - 1) {
                            timeToEnd--;
                            tvEndTimeAuthorized.setText(timeToEnd + ":00");
                            _listDataChild.get(listDataHeader[groupPosition])[1] = timeToEnd + ":00";
                        } else if (timeToStart >= timeToEnd - 1) {
                            //DO NOTHING
                        } else {
                            Toast.makeText(parent.getContext(), "Authorized time has to be between 6:00 - 23:00", Toast.LENGTH_LONG).show();
                        }
                        break;
                }
                tvEndDailyTime.setText((timeToEnd - timeToStart) + ":00");
                String minutesValue = tvStartDailyTime.getText().toString().split(":")[1];
                Double timeDaily = minutesValue.equals("30") ? Double.parseDouble(tvStartDailyTime.getText().toString().replace(":", ".")) : Double.parseDouble(tvStartDailyTime.getText().toString().split(":")[0]);
                if (timeToEnd - timeToStart < timeDaily) {
                    tvStartDailyTime.setText((timeToEnd - timeToStart) + ":00");
                    _listDataChild.get(listDataHeader[groupPosition])[2] = (timeToEnd - timeToStart) + ":00";
                }
                seekBar.setMax((timeToEnd - timeToStart) * 2);
            }
        };

        ibMoreStartTimeAuthorized.setOnClickListener(listenerMoreOrLess);
        ibLessStartTimeAuthorized.setOnClickListener(listenerMoreOrLess);
        ibMoreEndTimeAuthorized.setOnClickListener(listenerMoreOrLess);
        ibLessEndTimeAuthorized.setOnClickListener(listenerMoreOrLess);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    String maxHours = (progress < 20) ? "" + progress / 2 : "" + progress / 2;
                    String maxMinutes = (progress % 2 == 0) ? "00" : "" + 30;
                    String currentTime = maxHours + ":" + maxMinutes;
                    tvStartDailyTime.setText(currentTime);
                    _listDataChild.get(listDataHeader[groupPosition])[2] = currentTime;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader[groupPosition];
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.length;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View groupView, ViewGroup parent) {
        final String headerTitle = (String) getGroup(groupPosition);
        if (groupView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            groupView = infalInflater.inflate(R.layout.list_group, null);
        }

        final TextView tvDayOfWeek = (TextView) groupView.findViewById(R.id.tvDayOfWeek);
        final TextView tvTimeSet = (TextView) groupView.findViewById(R.id.tvEndAuthorizedHours);
        final CheckBox cbTimeControl = (CheckBox) groupView.findViewById(R.id.cbTimeControl);
        cbTimeControl.setFocusable(false);
        tvDayOfWeek.setFocusable(false);
        tvDayOfWeek.setTypeface(null, Typeface.BOLD);
        tvDayOfWeek.setText(headerTitle.substring(0, 3) + ".");
        if (cbTimeControl.isChecked()) {
            tvTimeSet.setText(_listDataChild.get(headerTitle)[0] + " - " + _listDataChild.get(headerTitle)[1]);
            tvDayOfWeek.setTextColor(groupView.getContext().getResources().getColor(R.color.colorAccent));
        }

        cbTimeControl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (cbTimeControl.isChecked() && groupPosition != 0) {
                    tvTimeSet.setVisibility(View.VISIBLE);
                    tvTimeSet.setText(_listDataChild.get(headerTitle)[0] + " - " + _listDataChild.get(headerTitle)[1]);
                    _listDataChild.get(headerTitle)[3] = "true";
                    tvTimeSet.setTextColor(buttonView.getContext().getResources().getColor(R.color.colorAccent));
                    tvDayOfWeek.setTextColor(buttonView.getContext().getResources().getColor(R.color.colorAccent));
                } else {
                    tvTimeSet.setText(_context.getString(R.string.time_not_defined));
                    _listDataChild.get(headerTitle)[3] = "false";
                    tvTimeSet.setTextColor(buttonView.getContext().getResources().getColor(R.color.colorPrimary));
                    tvDayOfWeek.setTextColor(buttonView.getContext().getResources().getColor(R.color.colorPrimary));
                }
            }
        });

        if (isExpanded) {
            tvTimeSet.setVisibility(View.INVISIBLE);
            cbTimeControl.setVisibility(View.INVISIBLE);
            tvDayOfWeek.setTextColor(groupView.getContext().getResources().getColor(R.color.colorAccent));
            tvDayOfWeek.setText(headerTitle);
        } else {
            tvTimeSet.setVisibility(View.INVISIBLE);
            cbTimeControl.setVisibility(View.VISIBLE);
            tvDayOfWeek.setText(headerTitle.substring(0, 3) + ".");
        }

        if (!Boolean.parseBoolean(_listDataChild.get(headerTitle)[3]) && groupPosition != 0) {
            cbTimeControl.setChecked(false);
            tvTimeSet.setTextColor(groupView.getContext().getResources().getColor(R.color.colorPrimary));
            tvDayOfWeek.setTextColor(groupView.getContext().getResources().getColor(R.color.colorPrimary));
        } else if (Boolean.parseBoolean(_listDataChild.get(headerTitle)[3]) && groupPosition != 0) {
            tvTimeSet.setVisibility(View.VISIBLE);
            cbTimeControl.setChecked(true);
            tvTimeSet.setTextColor(groupView.getContext().getResources().getColor(R.color.colorAccent));
            tvDayOfWeek.setTextColor(groupView.getContext().getResources().getColor(R.color.colorAccent));
        }

        //Everyday
        if (groupPosition == 0) {
            cbTimeControl.setVisibility(View.INVISIBLE);
            tvDayOfWeek.setText(headerTitle);
            _listDataChild.get(headerTitle)[3] = "false";
            tvTimeSet.setTextColor(groupView.getContext().getResources().getColor(R.color.colorAccent));
            tvDayOfWeek.setTextColor(groupView.getContext().getResources().getColor(R.color.colorAccent));
            tvTimeSet.setText(_listDataChild.get(headerTitle)[0] + " - " + _listDataChild.get(headerTitle)[1]);
        }

        return groupView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public HashMap<String, String[]> get_listDataChild() {
        return _listDataChild;
    }
}
