package interactive.cide.com.kuriocompagnon;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.nsd.NsdServiceInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import interactive.cide.com.kuriocompagnon.ui.timecontrol.TimeControlFragment;
import interactive.cide.com.kuriocompagnon.model.AppInfo;
import interactive.cide.com.kuriocompagnon.model.TimeSlotSetting;


public class BenMainActivity extends AppCompatActivity {

    private CompanionSocketClient companionSocketClient;
    private NsdHelper nsdHelper;
    private Handler updateHandler;
    private TextView statusView;
    private Button  batteryBtn, timeslotBtn, btTimeControl;
    public ArrayList<TimeSlotSetting> timeSlotSetting;
    private Handler uiHandler;
    private Button  appInfoBtn;

    private List<NsdServiceInfo> availableServiceInfo = new ArrayList<>();
    private Map<String,Button> cnxButtonMap = new HashMap<>();
    private static Map<String, Bitmap> iconMap = new HashMap<>();

    ListView iconlistView;

    private MessageManager messageManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        statusView = findViewById(R.id.status);
        iconlistView = (ListView) findViewById(R.id.icon_list);


        batteryBtn = findViewById(R.id.battery_btn);
        batteryBtn.setEnabled(false);
        batteryBtn.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              messageManager.requestObject(MessageManager.BATTERY);
                                          }
                                      }

        );

        timeslotBtn = findViewById(R.id.timeslot_btn);
        timeslotBtn.setEnabled(false);
        timeslotBtn.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               messageManager.requestObject(MessageManager.TIMESLOT);
                                           }
                                       }

        );

        appInfoBtn = findViewById(R.id.appInfo_btn);
        appInfoBtn.setEnabled(false);
        appInfoBtn.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              iconMap.clear(); //for test
                                              messageManager.requestObject(MessageManager.APPINFO);

                                          }
                                      }

        );
        nsdHelper = new NsdHelper(null,null);

        uiHandler = new BenMainActivity.SocketReplyHandler(this);
        companionSocketClient = new CompanionSocketClient(uiHandler);
        messageManager = new MessageManager(companionSocketClient);

        btTimeControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(statusView.getText() != null) {
                    Intent intent = new Intent(getApplicationContext(), TimeControlFragment.class);
                    try {
                        intent.putExtra("time_control",ObjectSerializer.serialize(timeSlotSetting));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    intent.putExtra("time_control", statusView.getText().toString());
                    startActivity(intent);
                } else
                    Toast.makeText(getApplicationContext(),"click on Time Slot button first",Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onResume() {

        nsdHelper.discoverServices();
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (nsdHelper!=null) {
            nsdHelper.stopDiscovery();
        }
        super.onPause();
    }



    @Override
    protected void onDestroy() {
        if (nsdHelper!=null) {
            nsdHelper.stopDiscovery();
        }
        super.onDestroy();
    }

    public void serviceReady() {
        batteryBtn.setEnabled(true);


    }

    public void serviceLost(String androidId) {
        batteryBtn.setEnabled(false);
        timeslotBtn.setEnabled(false);
        appInfoBtn.setEnabled(false);
        statusView.setText( "Disconnected !");
        iconlistView.setAdapter(new IconListAdapter(BenMainActivity.this, new ArrayList<AppInfo>()));
        nsdHelper.discoverServices();
        cnxButtonMap.get(androidId).setEnabled(false);
    }

//    public void serviceReady(final NsdServiceInfo serviceInfo) {
//
//
//        String name = new String(serviceInfo.getAttributes().get(NsdHelper.SERVICE_USER_NAME));
//        final Button cnxBtn = new Button(BenMainActivity.this);
//        cnxBtn.setText("Connect to "+name);
//        cnxBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                new AsyncTask<Void, Void, Boolean>() {
//                    @Override
//                    protected Boolean doInBackground(Void... voids) {
//
//                        return  companionSocketClient.createSocket(serviceInfo.getHost(), serviceInfo.getPort());
//                    }
//
//                    @Override
//                    protected void onPostExecute(Boolean result) {
//                        if (result) {
//                            batteryBtn.setEnabled(true);
//                            timeslotBtn.setEnabled(true);
//                            Toast.makeText(getApplicationContext(),"Connected !",Toast.LENGTH_SHORT).show();
//
//                        } else{
//                            Toast.makeText(getApplicationContext(),"ERROR: Can not connect to tablet...",Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }.execute((Void) null);
//
//            }
//        });
//
//
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
//
//
//                LinearLayout ll = (LinearLayout)findViewById(R.id.button_layout_bar);
//                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(96, LinearLayout.LayoutParams.WRAP_CONTENT);
//                ll.addView(cnxBtn, lp);
//            }
//        });
//    }

    public void serviceReady(final NsdServiceInfo serviceInfo) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                availableServiceInfo.add(serviceInfo);
                String name = NsdHelper.getUserName(serviceInfo);
                final String id = NsdHelper.getAndroidId(serviceInfo);
                Button cnxBtn = cnxButtonMap.get(id);
                if (cnxBtn == null) {
                    cnxBtn = new Button(BenMainActivity.this);
                    cnxBtn.setText("Connect to " + name);
                    cnxBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            new AsyncTask<Void, Void, Boolean>() {
                                @Override
                                protected Boolean doInBackground(Void... voids) {
                                    return messageManager.initializeManager(serviceInfo);
                                }

                                @Override
                                protected void onPostExecute(Boolean result) {
                                    if (result) {
                                        batteryBtn.setEnabled(true);
                                        timeslotBtn.setEnabled(true);
                                        appInfoBtn.setEnabled(true);
                                        statusView.setText("Connected !");
                                    } else {
                                        statusView.setText("\n" + "ERROR: Can not connect to tablet...");
                                    }
                                }
                            }.execute((Void) null);
                        }
                    });

                    LinearLayout ll = (LinearLayout) findViewById(R.id.button_layout_bar);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(96, LinearLayout.LayoutParams.WRAP_CONTENT);
                    ll.addView(cnxBtn, lp);
                    cnxButtonMap.put(id,cnxBtn);


                } else {
                    cnxBtn.setEnabled(true);
                }
            }});


    }

    public static Map<String, Bitmap> getIconMap() {
        return iconMap;
    }

    public MessageManager getMessageManager() {
        return messageManager;
    }


    public synchronized void refreshIconListView() {
        ((BaseAdapter)iconlistView.getAdapter()).notifyDataSetChanged();
    }


    private static class MyHandler extends Handler {
        private final BenMainActivity mainActivity;

        MyHandler(BenMainActivity mainActivity) {
            this.mainActivity = mainActivity;
        }

        @Override
        public void handleMessage(Message msg) {
            String reply = msg.getData().getString("msg");
            // mainActivity.statusView.append("\n" + reply);

            if (reply.startsWith("TimeSlotSetting")){
                mainActivity.timeSlotSetting = new ArrayList<>();
                mainActivity.timeSlotSetting.addAll((Collection<? extends TimeSlotSetting>) MessageDecoder.decode(reply));
            }
        }
    }

    class SocketReplyHandler extends Handler {
        private final BenMainActivity mainActivity;

        SocketReplyHandler(BenMainActivity mainActivity) {
            this.mainActivity = mainActivity;
        }


        @Override
        public void handleMessage(Message msg) {

            String type = msg.getData().getString("type");

            if (type.equals("error")) {
                String androidId = msg.getData().getString("id");
                mainActivity.serviceLost(androidId);
            }

            if (type.equals("object")) {

                String reply = msg.getData().getString("msg");
                mainActivity.statusView.setText(reply);
                Object obj = MessageDecoder.decode(reply);
                if (obj != null) {
                    mainActivity.statusView.append("\n" + "object decoded !"+obj.toString());
                    if (reply.startsWith(MessageManager.APPINFO)){
                        mainActivity.iconlistView.setAdapter(new IconListAdapter(mainActivity, (List<AppInfo>)obj));
                    }
                }
            }

            if (type.equals("icon")) {

                String packageName = msg.getData().getString("packageName");
                byte[] data = msg.getData().getByteArray("data");
                Bitmap bitmap = BitmapFactory.decodeByteArray(data,0,data.length);
                BenMainActivity.getIconMap().put(packageName,bitmap);
                mainActivity.refreshIconListView();
            }

        }
    }
}

