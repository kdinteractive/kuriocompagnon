package interactive.cide.com.kuriocompagnon;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.nsd.NsdServiceInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import interactive.cide.com.kuriocompagnon.model.AppInfo;


public class LoloMainActivity extends AppCompatActivity {


    private CompanionSocketClient companionSocketClient;
    private NsdHelper nsdHelper;
    private Handler uiHandler;
 ;
    private Button  batteryBtn;
    private Button timeslotBtn;
    private Button  appInfoBtn;

    private List<NsdServiceInfo> availableServiceInfo = new ArrayList<>();
    private Map<String,Button> cnxButtonMap = new HashMap<>();
    private static Map<String,Bitmap> iconMap = new HashMap<>();

    ListView iconlistView;
    TextView statusView;


    private MessageManager messageManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        statusView = findViewById(R.id.status);
        iconlistView = (ListView) findViewById(R.id.icon_list);


        batteryBtn = findViewById(R.id.battery_btn);
        batteryBtn.setEnabled(false);
        batteryBtn.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          messageManager.requestObject(MessageManager.BATTERY);
                                      }
                                  }

        );

        timeslotBtn = findViewById(R.id.timeslot_btn);
        timeslotBtn.setEnabled(false);
        timeslotBtn.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              messageManager.requestObject(MessageManager.TIMESLOT);
                                          }
                                      }

        );

        appInfoBtn = findViewById(R.id.appInfo_btn);
        appInfoBtn.setEnabled(false);
        appInfoBtn.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               iconMap.clear(); //for test
                                               messageManager.requestObject(MessageManager.APPINFO);

                                           }
                                       }

        );
        nsdHelper = new NsdHelper(null,null);

        uiHandler = new SocketReplyHandler(this);
        companionSocketClient = new CompanionSocketClient(uiHandler);
        messageManager = new MessageManager(companionSocketClient);

    }

    @Override
    protected void onResume() {

        nsdHelper.discoverServices();
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (nsdHelper!=null) {
            nsdHelper.stopDiscovery();
        }
        super.onPause();
    }



    @Override
    protected void onDestroy() {
        if (nsdHelper!=null) {
            nsdHelper.stopDiscovery();
        }
        super.onDestroy();
    }

    public void serviceReady() {
        batteryBtn.setEnabled(true);
        statusView.append("\n" + "Connected !");


    }

    public void serviceLost(String androidId) {
        batteryBtn.setEnabled(false);
        timeslotBtn.setEnabled(false);
        appInfoBtn.setEnabled(false);
        statusView.setText( "Disconnected !");
        iconlistView.setAdapter(new IconListAdapter(LoloMainActivity.this, new ArrayList<AppInfo>()));
        nsdHelper.discoverServices();
        cnxButtonMap.get(androidId).setEnabled(false);
    }


    public void serviceReady(final NsdServiceInfo serviceInfo) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                availableServiceInfo.add(serviceInfo);
                String name = NsdHelper.getUserName(serviceInfo);
                final String id = NsdHelper.getAndroidId(serviceInfo);
                Button cnxBtn = cnxButtonMap.get(id);
                if (cnxBtn == null) {
                    cnxBtn = new Button(LoloMainActivity.this);
                    cnxBtn.setText("Connect to " + name);
                    cnxBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            new AsyncTask<Void, Void, Boolean>() {
                                @Override
                                protected Boolean doInBackground(Void... voids) {
                                    return messageManager.initializeManager(serviceInfo);
                                }

                                @Override
                                protected void onPostExecute(Boolean result) {
                                    if (result) {
                                        batteryBtn.setEnabled(true);
                                        timeslotBtn.setEnabled(true);
                                        appInfoBtn.setEnabled(true);
                                        statusView.setText("Connected !");
                                    } else {
                                        statusView.setText("\n" + "ERROR: Can not connect to tablet...");
                                    }
                                }
                            }.execute((Void) null);
                        }
                    });

                    LinearLayout ll = (LinearLayout) findViewById(R.id.button_layout_bar);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(96, LinearLayout.LayoutParams.WRAP_CONTENT);
                    ll.addView(cnxBtn, lp);
                    cnxButtonMap.put(id,cnxBtn);


                } else {
                    cnxBtn.setEnabled(true);
                }
            }});


    }

    public static Map<String, Bitmap> getIconMap() {
        return iconMap;
    }

    public MessageManager getMessageManager() {
        return messageManager;
    }


    public synchronized void refreshIconListView() {
        ((BaseAdapter)iconlistView.getAdapter()).notifyDataSetChanged();
    }

    class SocketReplyHandler extends Handler {
        private final LoloMainActivity loloMainActivity;

        SocketReplyHandler(LoloMainActivity loloMainActivity) {
            this.loloMainActivity = loloMainActivity;
        }


        @Override
        public void handleMessage(Message msg) {

            String type = msg.getData().getString("type");

            if (type.equals("error")) {
                String androidId = msg.getData().getString("id");
                loloMainActivity.serviceLost(androidId);
            }

            if (type.equals("object")) {

                String reply = msg.getData().getString("msg");
                loloMainActivity.statusView.setText(reply);
                Object obj = MessageDecoder.decode(reply);
                if (obj != null) {
                    loloMainActivity.statusView.append("\n" + "object decoded !"+obj.toString());
                    if (reply.startsWith(MessageManager.APPINFO)){
                        loloMainActivity.iconlistView.setAdapter(new IconListAdapter(loloMainActivity, (List<AppInfo>)obj));
                    }
                }
            }

            if (type.equals("icon")) {

                String packageName = msg.getData().getString("packageName");
                byte[] data = msg.getData().getByteArray("data");
                Bitmap bitmap = BitmapFactory.decodeByteArray(data,0,data.length);
                LoloMainActivity.getIconMap().put(packageName,bitmap);
                loloMainActivity.refreshIconListView();
            }

        }
    }

}
