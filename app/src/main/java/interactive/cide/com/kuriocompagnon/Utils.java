package interactive.cide.com.kuriocompagnon;

import android.content.Context;

public class Utils {

    public static String getTimeStringFormat(Context context, int hours, int minutes) {
        String hourString = String.valueOf(hours);
        String minutesString = String.valueOf(minutes);
        if (hours < 10) {
            hourString = "0" + hourString;
        }
        if (minutes < 10) {
            minutesString = "0" + minutesString;
        }

        return context.getResources().getString(R.string.time_format_hour_minute, hourString, minutesString);
    }

}
