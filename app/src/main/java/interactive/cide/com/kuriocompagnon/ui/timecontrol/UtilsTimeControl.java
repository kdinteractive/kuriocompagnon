package interactive.cide.com.kuriocompagnon.ui.timecontrol;

public class UtilsTimeControl {

    public static enum Day{
        ALL("All Day",0),
        SUNDAY("Sunday",1),
        MONDAY("Monday",2),
        TUESDAY("Tuesday",3),
        WEDNESDAY("Wednesday",4),
        THURSDAY("Thursday",5),
        FRIDAY("Friday",6),
        SATURDAY("Saturday",7);


        private String name="";
        private int number;

        Day(String name,int number) {
            this.name = name;
            this.number=number;
        }

        public String getName() {
            return name;
        }

        public int getNumber() {
            return number;
        }

        @Override
        public String toString() {
            return  name;
        }
    }
}
