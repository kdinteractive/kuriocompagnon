package interactive.cide.com.kuriocompagnon;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.List;


import interactive.cide.com.kuriocompagnon.model.AppInfo;

public class IconListAdapter extends BaseAdapter {


    private final LayoutInflater layoutInflater;
    private final Context context;
    private List<AppInfo> listData;

    public IconListAdapter(Context context, List<AppInfo> obj) {
        this.listData = obj;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_layout, null);
            holder = new ViewHolder();
            holder.headlineView = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.thumbImage);
            holder.progressBar = convertView.findViewById(R.id.progressBar);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final AppInfo appinfo = (AppInfo) listData.get(position);
        holder.headlineView.setText(appinfo.getLabel());
        if (holder.imageView != null) {
            Bitmap icon = LoloMainActivity.getIconMap().get(appinfo.getName());
            if (icon!=null){
                holder.progressBar.setVisibility(View.INVISIBLE);
                holder.imageView.setImageBitmap(icon);
            }else {
                new Thread() {
                    @Override
                    public void run() {
                        ((LoloMainActivity)context).getMessageManager().requestIcon(appinfo.getName());
                    }
                }.start();
            }

        }
        return convertView;
    }

    static class ViewHolder {
        ProgressBar progressBar;
        TextView headlineView;
        ImageView imageView;
    }

}
