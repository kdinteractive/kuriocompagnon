package interactive.cide.com.kuriocompagnon;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import interactive.cide.com.kuriocompagnon.model.AppInfo;
import interactive.cide.com.kuriocompagnon.model.BatteryInfo;
import interactive.cide.com.kuriocompagnon.model.TimeSlotSetting;


public class MessageDecoder {

    private static final String UNKNOWN = "unknown";

    private static String extractJsonFromReply(String messageStr){
        return messageStr.substring(messageStr.indexOf("#")+1);
    }


    public static Object decode(String messageStr) {

        Gson gson = new Gson();
        String objMessage = extractJsonFromReply(messageStr);
        if (messageStr.startsWith(MessageManager.BATTERY)){
            BatteryInfo info = gson.fromJson(objMessage, BatteryInfo.class);
            return info;
        }
        if (messageStr.startsWith(MessageManager.TIMESLOT)){

            Type collectionType = new TypeToken<List<TimeSlotSetting>>(){}.getType();
            List<TimeSlotSetting> infoLst = gson.fromJson(objMessage, collectionType);
            return infoLst;
        }
        if (messageStr.startsWith(MessageManager.APPINFO)){

            Type collectionType = new TypeToken<List<AppInfo>>(){}.getType();
            List<AppInfo> infoLst = gson.fromJson(objMessage, collectionType);
            return infoLst;
        }

        if (messageStr.startsWith(UNKNOWN)){
            return new String(messageStr+" "+UNKNOWN+" by server");

        }
        return  new String("message can not be decoded...");
    }
}
