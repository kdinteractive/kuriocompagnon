package interactive.cide.com.kuriocompagnon.ui.timecontrol;


import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.HashMap;

import interactive.cide.com.kuriocompagnon.MainActivity;
import interactive.cide.com.kuriocompagnon.ui.appmanagement.AppManagement;
import interactive.cide.com.kuriocompagnon.R;


public class TimeControlFragment extends Fragment {

    public static String VALUE;
    public static ArrayList<TimeSlotSetting> tc_time_slot_setting;
    private SharedPreferences sharedPreferences;
    private ArrayList<TimeSlotSetting> listTimeSlotSettings;
    private TimeSlotSetting timeSlotSetting;
    private Switch swTimeControl;
    TCExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    public String[] dayList = {"Everyday","Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
    String[] timeValues = {"06:00", "23:00"};
    HashMap<String, String[]> listData, newListData;
    CheckBox cbTimeControl;
    ImageView ivBack;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private OnFragmentInteractionListener mListener;

    public TimeControlFragment() {
        // Required empty public constructor
    }

    public static TimeControlFragment newInstance(String param1, String param2) {
        TimeControlFragment fragment = new TimeControlFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_time_control, container, false);
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // ((MainActivity)getContext()).
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sharedPreferences = view.getContext().getSharedPreferences("CHILDINFO", 0);
        expListView = view.findViewById(R.id.lvExp);
        swTimeControl = view.findViewById(R.id.swTimeControl);

        listTimeSlotSettings = new ArrayList<>();
        tc_time_slot_setting = new ArrayList<>();
        listData = new HashMap<String, String[]>();
        newListData = new HashMap<String, String[]>();

        if (tc_time_slot_setting.isEmpty()) {
//            try {
//                tc_time_slot_setting.addAll((Collection<? extends TimeSlotSetting>) ObjectSerializer.deserialize(getIntent().getStringExtra("time_control")));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
//        VALUE = getContext().getIntent().getStringExtra("time_control");


        // preparing list data
        for (int i = 0; i < dayList.length; i++) {
            listData.put(dayList[i], sharedPreferences.getString("timeAuthorized" + dayList[i], "6:00 - 23:00 - 2:00 - false").split(" - "));
        }
        listAdapter = new TCExpandableListAdapter(getContext(), dayList, listData, expListView);

        // setting list adapter
        expListView.setGroupIndicator(null);
        expListView.setChildIndicator(null);
        expListView.setChildDivider(getResources().getDrawable(R.color.kurio_white));
        expListView.setDivider(getResources().getDrawable(R.color.colorPrimary));
        expListView.setDividerHeight(2);
        expListView.setAdapter(listAdapter);

        swTimeControl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    buttonView.setText("ON");
                    expListView.setEnabled(true);
                    expListView.setAlpha(1f);
                } else {
                    buttonView.setText("OFF");
                    for (int i = 0; i < dayList.length; i++) {
                        expListView.collapseGroup(i);
                    }
                    expListView.setEnabled(false);
                    expListView.setAlpha(0.5f);
                }
            }
        });

        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                for (int i = 0; i < dayList.length; i++) {
                    if (i != groupPosition && expListView.isGroupExpanded(i))
                        expListView.collapseGroup(i);
                }
            }
        });

        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                newListData = listAdapter.get_listDataChild();
                listData.put(dayList[groupPosition], newListData.get(dayList[groupPosition]));
                listAdapter.notifyDataSetChanged();
            }
        });

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                return false;
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(sharedPreferences.getBoolean("TimeControle",false)) {
            swTimeControl.setChecked(true);
            swTimeControl.setText("ON");
            expListView.setEnabled(true);
            expListView.setAlpha(1f);
        } else {
            swTimeControl.setChecked(false);
            swTimeControl.setText("OFF");
            expListView.setEnabled(false);
            expListView.setAlpha(0.5f);
        }
        for (int i = 0; i < dayList.length; i++) {
            listData.put(dayList[i], sharedPreferences.getString("timeAuthorized" + dayList[i], "6:00 - 23:00 - 2:00 - false").split(" - "));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        newListData = listAdapter.get_listDataChild();
        for(String day : dayList) {
            if(!Boolean.parseBoolean(newListData.get(day)[3])) {
                listData.put(day, newListData.get("Everyday"));
            } else {
                listData.put(day, newListData.get(day));
            }
        }
        listAdapter.notifyDataSetChanged();
        for (int i = 0; i < dayList.length; i++) {
            sharedPreferences.edit().putString("timeAuthorized" + dayList[i], listData.get(dayList[i])[0] + " - " + listData.get(dayList[i])[1] + " - " + listData.get(dayList[i])[2] + " - " + listData.get(dayList[i])[3]).apply();
        }
        sharedPreferences.edit().putBoolean("TimeControle",swTimeControl.isChecked()).apply();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppManagement.OnFragmentInteractionListener) {
            mListener = (TimeControlFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
