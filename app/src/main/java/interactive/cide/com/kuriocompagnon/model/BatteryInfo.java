package interactive.cide.com.kuriocompagnon.model;

public class BatteryInfo {
    boolean isCharging;
    int chargeLevel;
    int chargeScale;
    boolean usbCharge;
    boolean acCharge;

    @Override
    public String toString() {
        return "BatteryInfo{" +
                "isCharging=" + isCharging +
                ", chargeLevel=" + chargeLevel +
                ", chargeScale=" + chargeScale +
                ", usbCharge=" + usbCharge +
                ", acCharge=" + acCharge +
                '}';
    }
}
