package interactive.cide.com.kuriocompagnon.ui.association;


import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import interactive.cide.com.kuriocompagnon.MainActivity;
import interactive.cide.com.kuriocompagnon.NsdHelper;
import interactive.cide.com.kuriocompagnon.R;



public class AssociationFragment extends Fragment {
    private List<NsdServiceInfo> availableServiceInfo = new ArrayList<>();
    private ListView childKurioListView;


    public AssociationFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        NsdHelper nsdHelper = ((MainActivity)getContext()).getNsdHelper();
        availableServiceInfo.addAll(nsdHelper.getServiceInfoMap().values());

        nsdHelper.addServiceListener( new ServiceListener() {
            @Override
            public void onServiceReady(final NsdServiceInfo serviceInfo) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!availableServiceInfo.contains(serviceInfo)) {
                            availableServiceInfo.add(serviceInfo);
                            childKurioListView.setAdapter(new KurioDeviceListAdapter(AssociationFragment.this.getContext(),availableServiceInfo));
                        }
                    }
                });
            };
        });


        nsdHelper.discoverServices();


     /*   NsdHelper nsdHelper = new NsdHelper(((MainActivity)getContext()).getNsdManager(), new ServiceListener() {
            @Override
            public void onServiceReady(final NsdServiceInfo serviceInfo) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        availableServiceInfo.add(serviceInfo);
                        childKurioListView.setAdapter(new KurioDeviceListAdapter(AssociationFragment.this.getContext(),availableServiceInfo));
                    }
                });
            }
        });
        nsdHelper.discoverServices();

*/
        //((MainActivity)getContext()).getNsdHelper().discoverServices();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_association, container, false);
        childKurioListView = view.findViewById(R.id.childKurioList);
        childKurioListView.setAdapter(new KurioDeviceListAdapter(getContext(),availableServiceInfo));
        return view;
        
    }

}
