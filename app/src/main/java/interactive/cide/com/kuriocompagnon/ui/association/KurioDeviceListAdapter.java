package interactive.cide.com.kuriocompagnon.ui.association;

import android.content.Context;
import android.net.nsd.NsdServiceInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import interactive.cide.com.kuriocompagnon.MainActivity;
import interactive.cide.com.kuriocompagnon.NsdHelper;
import interactive.cide.com.kuriocompagnon.R;

public class KurioDeviceListAdapter extends BaseAdapter {


    private final LayoutInflater layoutInflater;
    private final Context context;
    private List<NsdServiceInfo> nsdServiceInfos;

    public KurioDeviceListAdapter(Context context, List<NsdServiceInfo> obj) {
        this.nsdServiceInfos = obj;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }



    @Override
    public int getCount() {
        return nsdServiceInfos.size();
    }

    @Override
    public NsdServiceInfo getItem(int position) {
        return nsdServiceInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.kurio_device_row_layout, null);
            holder = new ViewHolder();
            holder.deviceNameView = (TextView) convertView.findViewById(R.id.device_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        final Button connectionButton = convertView.findViewById(R.id.assoc_button);
        final Button revokeButton = convertView.findViewById(R.id.revoke_button);

        connectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).requestAssociation(getItem(position));
            }
        });

        revokeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).revokeAssociation(getItem(position));
                connectionButton.setEnabled(true);
                revokeButton.setEnabled(false);
            }
        });

        NsdServiceInfo serviceInfo = (NsdServiceInfo) nsdServiceInfos.get(position);
        String name = NsdHelper.getUserName(serviceInfo);
        String id = NsdHelper.getAndroidId(serviceInfo);

        if (((MainActivity)context).getAuthorizedDevice().contains(id)){
            connectionButton.setEnabled(false);
            revokeButton.setEnabled(true);
        }else{
            connectionButton.setEnabled(true);
            revokeButton.setEnabled(false);
        };

        holder.deviceNameView.setText("user:"+name+" id:"+id+"ip:"+ serviceInfo.getHost().getHostAddress());
        return convertView;
    }

    static class ViewHolder {
        TextView deviceNameView;
    }

}
