/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package interactive.cide.com.kuriocompagnon;

import android.content.Context;
import android.net.nsd.NsdServiceInfo;
import android.net.nsd.NsdManager;
import android.util.Log;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import interactive.cide.com.kuriocompagnon.ui.association.ServiceListener;

public class NsdHelper {


    private static final String SERVICE_TYPE = "_http._tcp.";
    private static final String ACCEPTED_SERVICE_NAME = "KurioCompanionServiceServer";

    private static final String TAG = "NsdHelper";
    private List<ServiceListener> serviceListeners = new ArrayList<>();


    private NsdManager nsdManager;
    private NsdManager.DiscoveryListener discoveryListener;



    private Map<String,NsdServiceInfo> serviceInfoMap = new HashMap<>();

    private static final String SERVICE_USER_NAME = "NAME";
    private static final String SERVICE_ANDROID_ID = "ID";



    public NsdHelper(Context context, ServiceListener serviceListener) {
        addServiceListener(serviceListener);
        nsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);

    }

    public void addServiceListener(ServiceListener serviceListener){
        serviceListeners.add(serviceListener);
    }


    public void discoverServices() {
        stopDiscovery();  // Cancel any existing discovery request
        startDiscovery();

    }

    public void stopDiscovery() {
        if (discoveryListener != null) {
            nsdManager.stopServiceDiscovery(discoveryListener);
            discoveryListener = null;
        }
    }

    private void startDiscovery() {

        discoveryListener = new NsdManager.DiscoveryListener() {

            @Override
            public void onDiscoveryStarted(String regType) {
                Log.d(TAG, "Service discovery started");
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                // A service was found! Do something with it.
                Log.d(TAG, "Service Found: " + service);

                if (!service.getServiceType().equals(SERVICE_TYPE)) {
                    Log.e(TAG, "Unknown Service Type: " + service.getServiceType());
                }  else if ( !service.getServiceName().startsWith(ACCEPTED_SERVICE_NAME)){
                    Log.e(TAG, "Unknown Service Name: " + service.getServiceName());
                }else {
                    startResolveService(service);
                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo service) {
                Log.e(TAG, "service lost" + service);
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.i(TAG, "Discovery stopped: " + serviceType);
            }

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
            }
        };
        nsdManager.discoverServices( SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, discoveryListener);
    }


    private void startResolveService(NsdServiceInfo serviceInfoToResolve){
        NsdManager.ResolveListener newResolveListener = new NsdManager.ResolveListener() {
            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                Log.e(TAG, "Resolve Failed: " + serviceInfo + "\tError Code: " + errorCode);
                switch (errorCode) {
                    case NsdManager.FAILURE_ALREADY_ACTIVE:
                        Log.e(TAG, "FAILURE_ALREADY_ACTIVE");
                        // Just try again... or do nothing ...
                       // startResolveService(serviceInfo);
                        break;
                    case NsdManager.FAILURE_INTERNAL_ERROR:
                        Log.e(TAG, "FAILURE_INTERNAL_ERROR");
                        break;
                    case NsdManager.FAILURE_MAX_LIMIT:
                        Log.e(TAG, "FAILURE_MAX_LIMIT");
                        break;
                }
            }

            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {
                Log.i(TAG, "Service Resolved: " + serviceInfo);
                Map<String, byte[]> attributes = serviceInfo.getAttributes();
                String name = new String(attributes.get(SERVICE_USER_NAME));
                String id = new String(attributes.get(SERVICE_ANDROID_ID));
                NsdServiceInfo service = serviceInfoMap.get(id);
                if (service==null) {
                    Log.d(TAG,"Add Service from KurioTablet of "+name+"("+id+")");
                    serviceInfoMap.put(id,serviceInfo);
                    for (ServiceListener listener:serviceListeners){
                        listener.onServiceReady(serviceInfo);
                    }

                }else {
                    if (service.getServiceName().equals(serviceInfo.getServiceName()) && service.getPort()==serviceInfo.getPort()) {
                        Log.d(TAG,"Service Already register");
                        //serviceListener.onServiceReady(serviceInfo);
                    }else{
                        //this is very coner case when the child device have change his ip
                        Log.e(TAG, "device have change IP, socket must be recreated");
                       // ((LoloMainActivity) context).serviceLost();
                       //serviceInfoMap.put(id,serviceInfo);
                        //todo : do something


                    }
                }
            }
        };
        nsdManager.resolveService(serviceInfoToResolve, newResolveListener);
     }

    public Map<String, NsdServiceInfo> getServiceInfoMap() {
        return serviceInfoMap;
    }

    public static String getUserName(NsdServiceInfo serviceInfo) {
        return new String(serviceInfo.getAttributes().get(NsdHelper.SERVICE_USER_NAME));
    }

    public static String getAndroidId(NsdServiceInfo serviceInfo) {
        return new String(serviceInfo.getAttributes().get(NsdHelper.SERVICE_ANDROID_ID));
    }


}
