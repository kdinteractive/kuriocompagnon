package interactive.cide.com.kuriocompagnon;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;

import java.net.Socket;
import java.net.UnknownHostException;

public class CompanionSocketClient {

    private final Handler updateHandler;

    private Socket socket;
    private InetAddress hostAddress;
    private int hostPort;

    private static final String TAG = "SocketClient";
    private boolean isSocketConnectedCreated;


    public CompanionSocketClient(Handler updateHandler) {
        this.updateHandler = updateHandler;
    }

    boolean createSocket( InetAddress hostAddress,int hostPort){
        this.hostAddress =hostAddress;
        this.hostPort = hostPort;
        isSocketConnectedCreated = false;

        try {
            if (getSocket() == null) {

                setSocket(new Socket(hostAddress, hostPort));
                Log.d(TAG, "Client-side socket initialized.");

            } else {
                Log.d(TAG, "Socket already initialized. skipping!");
            }

            Thread messageReceiverThread = new Thread(new ReceivingThread());
            messageReceiverThread.start();
            isSocketConnectedCreated = true;



        } catch (UnknownHostException e) {
            Log.d(TAG, "Initializing socket failed, UHE", e);
        } catch (IOException e) {
            Log.d(TAG, "Initializing socket failed, IOE.", e);
        }
        return isSocketConnectedCreated;

    }

    private void setSocket(Socket socket) {
        this.socket = socket;
    }

    public Socket getSocket() {
        return socket;
    }

    void closeSocket(){
        try {
            getSocket().close();
            setSocket(null);
        } catch (IOException e) {
            Log.e(TAG, "close socket failed, IOE.", e);
        }
    }

    private synchronized void updateMessages(String msg) {

        Bundle messageBundle = new Bundle();
        messageBundle.putString("type", "object");
        messageBundle.putString("msg", msg);

        Message message = new Message();
        message.setData(messageBundle);
        updateHandler.sendMessage(message);

    }

    private void updateAssociation(String messageStr) {
        Bundle messageBundle = new Bundle();
        messageBundle.putString("type", "association");
        String id = messageStr.substring(messageStr.indexOf("#")+1);
        messageBundle.putString("id", id);

        Message message = new Message();
        message.setData(messageBundle);
        updateHandler.sendMessage(message);

    }

    private synchronized void updateError(String id) {

        Bundle messageBundle = new Bundle();
        messageBundle.putString("type", "error");
        messageBundle.putString("id", id);

        Message message = new Message();
        message.setData(messageBundle);
        updateHandler.sendMessage(message);

    }

    private synchronized void updateIcon(String packageName, byte[] data) {
        Bundle messageBundle = new Bundle();
        messageBundle.putString("type", "icon");
        messageBundle.putString("packageName",packageName);
        messageBundle.putByteArray("data",data);

        Message message = new Message();
        message.setData(messageBundle);
        updateHandler.sendMessage(message);

    }

    class ReceivingThread implements Runnable {
        String androidId;


        @Override
        public void run() {

            BufferedReader input;
            try {

                input = new BufferedReader(new InputStreamReader(
                        socket.getInputStream()));
                while (!Thread.currentThread().isInterrupted()) {

                    String messageStr = input.readLine();

                    if (messageStr != null) {
                        Log.d(TAG, "Message received");
                        Log.v(TAG, "Read from the stream: " + messageStr);
                        if (messageStr.startsWith("Icon")){
                            String packageName = messageStr.substring(messageStr.indexOf("@")+1,messageStr.indexOf("#"));
                            String encodedIcon = messageStr.substring(messageStr.indexOf("#")+1);
                            Gson gson = new Gson();
                            String value = gson.fromJson(encodedIcon,String.class);
                            byte[] data = Base64.decode(value,Base64.DEFAULT);
                            updateIcon(packageName,data);

                        }else if (messageStr.startsWith("association")){
                            updateAssociation(messageStr);
                        }else{
                            updateMessages(messageStr);
                        }
                    } else {
                        Log.d(TAG, "The nulls! The nulls!");
                        break;
                    }
                }
                input.close();

            } catch (IOException e) {
                Log.e(TAG, "Server loop error: ", e);
                updateError(e.getMessage());
                setSocket(null);
            }
        }
    }




}
