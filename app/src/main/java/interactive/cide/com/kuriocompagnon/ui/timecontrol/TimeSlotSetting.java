package interactive.cide.com.kuriocompagnon.ui.timecontrol;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

/**
 * Created by lionel on 05/12/14.
 */
public class TimeSlotSetting {

    private int mDay;

    private int mSessionTime;
    private int mPlayTime;
    private int mRestTime;
    private ArrayList<String[]> mTimeSlot = new ArrayList<>();

    private boolean mEnabled;



    public TimeSlotSetting() {
        mDay = 0;
        mSessionTime = 1440;
        mPlayTime = 30;
        mRestTime = 1440;
        mTimeSlot.add(new String[]{"6:00","23:00"});
        mEnabled = false;

    }

    public TimeSlotSetting(TimeSlotSetting ts) {
        mDay = ts.getDay();
        mSessionTime = ts.getSessionTime();
        mPlayTime = ts.getPlayTime();
        mRestTime = ts.getRestTime();
        mTimeSlot = ts.getTimeSlot();
        mEnabled = ts.isEnabled();

    }

    public TimeSlotSetting(int day, int sessionTime, int playTime, int restTime,
                           ArrayList<String[]> timeSlot, boolean enabled) {
        mDay = day;
        mSessionTime = sessionTime;
        mPlayTime = playTime;
        mRestTime = restTime;
        mTimeSlot = timeSlot;
        mEnabled = enabled;



    }

    public TimeSlotSetting(int day, int sessionTime, int playTime, int restTime,
                           String timeSlot, boolean enabled) {
        this(day, sessionTime, playTime, restTime, getTimeSlotFromString(timeSlot), enabled);
    }



    private static ArrayList<String[]> getTimeSlotFromString(String timeSlot) {
        ArrayList<String[]> allTimeSlot = new ArrayList<>();

        String[] split = timeSlot.split("@");
        for (String value : split) {
            String[] splitTimeSlot = value.split("-");
            allTimeSlot.add(splitTimeSlot);
        }
        return allTimeSlot;
    }

    public int getDay() {
        return mDay;
    }

    public void setDay(int day) {
        mDay = day;
    }


    public int getSessionTime() {
        return mSessionTime;
    }

    public void setSessionTime(int sessionTime) {
        mSessionTime = sessionTime;
    }

    public int getPlayTime() {
        return mPlayTime;
    }

    public void setPlayTime(int playTime) {
        mPlayTime = playTime;
    }

    public int getRestTime() {
        return mRestTime;
    }

    public void setRestTime(int restTime) {
        mRestTime = restTime;
    }



    /**
     * 0 for start time
     * 1 for end time
     *
     * @return
     */
    public ArrayList<String[]> getTimeSlot() {
        return mTimeSlot;
    }

    public void setTimeSlot(ArrayList<String[]> timeSlot) {
        mTimeSlot = timeSlot;
    }

//    public boolean isException() {
//        return mException;
//    }

//    public void setException(boolean exception) {
//        mException = exception;
//    }

    public boolean isEnabled() {
        return mEnabled;
    }

    public void setEnabled(boolean enabled) {
        mEnabled = enabled;
    }

    public String getTimeSlotAsString() {

        String timeSlotAsString = "";
        for (int i = 0; i < mTimeSlot.size(); i++) {
            String[] timeSlot = mTimeSlot.get(i);

            timeSlotAsString = timeSlotAsString.concat(timeSlot[0] + '-' + timeSlot[1]);

            if (i < (mTimeSlot.size() - 1)) {
                timeSlotAsString = timeSlotAsString.concat("@");
            }
        }
        return timeSlotAsString;
    }

    public void addTimeSlot(String timeSlot) {
        String[] split = timeSlot.split("@");

        for (String value : split) {
            String[] valueSplit = value.split("-");
            mTimeSlot.add(valueSplit);
        }
    }

    public void setTimeSlot(String timeSlot) {
        String[] split = timeSlot.split("@");
        mTimeSlot.clear();
        for (String value : split) {
            String[] valueSplit = value.split("-");
            mTimeSlot.add(valueSplit);
        }
    }

    public String toJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

    public static void saveTimeSlot(TimeSlotSetting mTimeSlotSetting,Context context) {
        SharedPreferences sharedPreferences=context.getSharedPreferences("CHILDINFO", 0);
        sharedPreferences.edit().putString("DAY"+mTimeSlotSetting.getDay(),mTimeSlotSetting.toJson()).apply();
    }

    public static TimeSlotSetting getFromDay(int day,Context context) {
        SharedPreferences sharedPreferences=context.getSharedPreferences("CHILDINFO", 0);
        String timeSetting=sharedPreferences.getString("DAY"+day,new TimeSlotSetting().toJson());
        Gson gson=new Gson();
        TimeSlotSetting timeSlotSetting= gson.fromJson(timeSetting,TimeSlotSetting.class);
        timeSlotSetting.setDay(day);
        return timeSlotSetting;
    }
}
