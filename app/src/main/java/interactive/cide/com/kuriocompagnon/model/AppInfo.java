package interactive.cide.com.kuriocompagnon.model;

public class AppInfo {
    private String label;
    private String name;
    private boolean authorised;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAuthorised() {
        return authorised;
    }

    public void setAuthorised(boolean authorised) {
        this.authorised = authorised;
    }

    @Override
    public String toString() {
        return "AppInfo{" +
                "label='" + label + '\'' +
                ", name='" + name + '\'' +
                ", authorised=" + authorised +
                '}';
    }
}
