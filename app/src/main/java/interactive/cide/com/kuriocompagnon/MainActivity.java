package interactive.cide.com.kuriocompagnon;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import interactive.cide.com.kuriocompagnon.ui.ChildProfile;
import interactive.cide.com.kuriocompagnon.ui.appmanagement.AppManagement;

import interactive.cide.com.kuriocompagnon.ui.association.ServiceListener;
import interactive.cide.com.kuriocompagnon.ui.timecontrol.TimeControlFragment;
import interactive.cide.com.kuriocompagnon.ui.webfilter.WebFilter;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AppManagement.OnFragmentInteractionListener, WebFilter.OnFragmentInteractionListener, TimeControlFragment.OnFragmentInteractionListener {

    private AppBarConfiguration appBarConfiguration;
    private String[] children = {"Mike-boy", "Joanna-girl", "Sam-boy"};
    private ArrayList<ChildProfile> childProfiles;
    private NavController navController;
    private Menu menu;
    private NavigationView navigationView;

    private SharedPreferences pref;


    private SocketReplyHandler uiHandler;
    private CompanionSocketClient companionSocketClient;
    private MessageManager messageManager;

    private NsdHelper nsdHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        menu = navigationView.getMenu();
        childProfiles = new ArrayList<ChildProfile>();

        populateNavDrawer();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_home)
                .setDrawerLayout(drawer)
                .build();

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        //This handler is use for communicate bettwen the socket and the UI
        uiHandler = new SocketReplyHandler(this);

        companionSocketClient = new CompanionSocketClient(uiHandler);
        messageManager = new MessageManager(companionSocketClient);
        nsdHelper = new NsdHelper(this, new ServiceListener() {
            @Override
            public void onServiceReady(NsdServiceInfo serviceInfo) {
                autoConnect(serviceInfo);
            }
        });

        nsdHelper.discoverServices();
    }


    private void autoConnect(NsdServiceInfo serviceInfo) {

        Set<String> authorizedDeviceList = getAuthorizedDevice();
        if (authorizedDeviceList.isEmpty()) {
            //there are no device ( first launch ) so redirect user to Association
            navController.navigate(R.id.nav_association);
        } else {
            String authorizedId = authorizedDeviceList.iterator().next();
            String name = NsdHelper.getUserName(serviceInfo);
            String id = NsdHelper.getAndroidId(serviceInfo);

            //todo:  beter to connect to last authorizedId connected
            //try to connect to first authorizedId detected.
            if (authorizedId.equals(id)) {

                new AsyncTask<NsdServiceInfo, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(NsdServiceInfo... serviceInfos) {
                        return messageManager.initializeManager(serviceInfos[0]);
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        if (result) {
                            Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT);
                            Fragment navHostFragment = getSupportFragmentManager().findFragmentById(R.id.nav_home);
                          //  navHostFragment.getActivity().getActionBar().setTitle("aejaeijfija");

                        } else {
                            Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT);
                        }
                    }
                }.execute(serviceInfo);
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private void populateNavDrawer() {
        for (int i = 0; i < children.length; i++) {
            childProfiles.add(new ChildProfile(children[i].split("-")[0], children[i].split("-")[1], null));
            int drawableBoyGirl = childProfiles.get(i).getGender().equals("boy") ? R.drawable.avatar_07 : R.drawable.avatar_02;
            menu.add(0, R.id.nav_host_fragment, i, childProfiles.get(i).getName()).setIcon(getResources().getDrawable(drawableBoyGirl));
//                    .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        navController.navigate(R.id.nav_home);
//                        Toast.makeText(MainActivity.this, "Welcome to "+childProfiles.get(finalI).getName()+" profile", Toast.LENGTH_SHORT).show();
//                        return false;
//                    }
//                });
        }
    }

    public void requestAssociation(NsdServiceInfo item) {
        messageManager.requestAssociation(item);
    }

    public Set<String> getAuthorizedDevice() {
        if (pref==null) {
            //lazy loading
            pref = PreferenceManager.getDefaultSharedPreferences(this);
        }
        return pref.getStringSet("authorizedDeviceList", new HashSet<String>());
    }

    public NsdHelper getNsdHelper() {
       return nsdHelper;
    }

    public void revokeAssociation(NsdServiceInfo serviceInfo) {
        Set<String> authorizedDeviceList = getAuthorizedDevice();
        authorizedDeviceList.remove(NsdHelper.getAndroidId(serviceInfo));
        SharedPreferences.Editor editor= pref.edit();
        editor.putStringSet("authorizedDeviceList",authorizedDeviceList);
        editor.commit();
        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {
                navController.navigate(R.id.nav_home);
            }
        });*/
    }



    class SocketReplyHandler extends Handler {
        private final MainActivity mainActivity;

        SocketReplyHandler(MainActivity mainActivity) {
            this.mainActivity = mainActivity;
        }


        @Override
        public void handleMessage(Message msg) {

            String type = msg.getData().getString("type");
            if (type.equals("association")) {
                //sauvegarde l'id de la tablette qui a ete associe

                 String androidId = msg.getData().getString("id");
                 Set<String> authorizedDeviceList = getAuthorizedDevice();
                 authorizedDeviceList.add(androidId);
                 SharedPreferences.Editor editor= pref.edit();
                 editor.putStringSet("authorizedDeviceList",authorizedDeviceList);
                 editor.commit();
                 navController.navigate(R.id.nav_home);
            }

            if (type.equals("error")) {
                String androidId = msg.getData().getString("id");
            }

            if (type.equals("object")) {

                String reply = msg.getData().getString("msg");
                Object obj = MessageDecoder.decode(reply);
                if (obj != null) {
                    //mainActivity.statusView.append("\n" + "object decoded !"+obj.toString());

                    if (reply.startsWith(MessageManager.APPINFO)){
                        //mainActivity.iconlistView.setAdapter(new IconListAdapter(mainActivity, (List<AppInfo>)obj));
                    }
                }
            }

            if (type.equals("icon")) {

                String packageName = msg.getData().getString("packageName");
                byte[] data = msg.getData().getByteArray("data");
                Bitmap bitmap = BitmapFactory.decodeByteArray(data,0,data.length);
                //LoloMainActivity.getIconMap().put(packageName,bitmap);
                //loloMainActivity.refreshIconListView();
            }

        }
    }
}
