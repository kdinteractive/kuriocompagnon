package interactive.cide.com.kuriocompagnon.model;

import java.util.ArrayList;

public class TimeSlotSetting {
    private int mDay;

    private int mSessionTime;
    private int mPlayTime;
    private int mRestTime;
    private ArrayList<String[]> mTimeSlot = new ArrayList<>();

    private boolean mEnabled;

    @Override
    public String toString() {
        return "TimeSlotSetting{" +
                "mDay=" + mDay +
                ", mSessionTime=" + mSessionTime +
                ", mPlayTime=" + mPlayTime +
                ", mRestTime=" + mRestTime +
                ", mTimeSlot=" + mTimeSlot.get(0)[0] +"->"+ mTimeSlot.get(0)[1]+
                ", mEnabled=" + mEnabled +
                '}';
    }
}
