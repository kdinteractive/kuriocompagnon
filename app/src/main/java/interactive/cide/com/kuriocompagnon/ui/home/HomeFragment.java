package interactive.cide.com.kuriocompagnon.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import interactive.cide.com.kuriocompagnon.R;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private Fragment fragment;
    private CardView cardViewTC, cardViewAM, cardViewWF;
    private Switch swTimeControl, swAppManagement, swWebFilter;
    private HashMap<MutableLiveData<Integer>, MutableLiveData<Boolean>> mutableLiveDataSwitch;
    private List<Switch> boolSwitch;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_home, container, false);
        if (boolSwitch == null) boolSwitch = new ArrayList<>();
        cardViewTC = root.findViewById(R.id.cvTimeControl);
        cardViewAM = root.findViewById(R.id.cvAppManagement);
        cardViewWF = root.findViewById(R.id.cvWebFilter);
        swTimeControl = root.findViewById(R.id.swTimeControl2);
        swAppManagement = root.findViewById(R.id.swAppManagement);
        swWebFilter = root.findViewById(R.id.swWebFilter);


        CompoundButton.OnCheckedChangeListener switchListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setCardsView((Switch) buttonView);
                boolean isInSwitchList = false;
                for (int i = 0; i < boolSwitch.size(); i++) {
                    if (boolSwitch.get(i).getId() == buttonView.getId()) {
                        boolSwitch.set(i, (Switch) buttonView);
                        isInSwitchList = true;
                    }
                }
                if (!isInSwitchList) {
                    boolSwitch.add((Switch) buttonView);
                }
                homeViewModel.setSwitchValue(boolSwitch);
            }
        };

        View.OnClickListener cardListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                if (v.getId() == R.id.cvTimeControl) {
                    navController.navigate(R.id.nav_time_control);
                } else if (v.getId() == R.id.cvAppManagement) {
                    navController.navigate(R.id.nav_app_management);
                } else if (v.getId() == R.id.cvWebFilter) {
                    navController.navigate(R.id.nav_web_filter);
                }
            }
        };

        swTimeControl.setOnCheckedChangeListener(switchListener);
        swAppManagement.setOnCheckedChangeListener(switchListener);
        swWebFilter.setOnCheckedChangeListener(switchListener);

        cardViewTC.setOnClickListener(cardListener);
        cardViewAM.setOnClickListener(cardListener);
        cardViewWF.setOnClickListener(cardListener);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        HomeViewModel.Factory factory = new HomeViewModel.Factory(requireActivity().getApplication(), boolSwitch);
        homeViewModel = new ViewModelProvider(this, factory).get(HomeViewModel.class);
        subscribeToModel(homeViewModel);
    }

    private void subscribeToModel(final HomeViewModel model) {
        model.getSwitchValue().observe(getViewLifecycleOwner(), new Observer<List<Switch>>() {
            @Override
            public void onChanged(List<Switch> aSwitch) {
                for (Switch swSelected : aSwitch) {
                    setCardsView(swSelected);
                }
            }
        });
    }

    private void setCardsView(Switch switchToSet) {
        CardView cvSelected = null;
        switch (switchToSet.getId()) {
            case R.id.swTimeControl2:
                cvSelected = cardViewTC;
                break;
            case R.id.swWebFilter:
                cvSelected = cardViewWF;
                break;
            case R.id.swAppManagement:
                cvSelected = cardViewAM;
                break;
        }
        if (switchToSet.isChecked()) {
            switchToSet.setText("ON");
            cvSelected.setEnabled(true);
            cvSelected.setAlpha(1f);
        } else {
            switchToSet.setText("OFF");
            cvSelected.setEnabled(false);
            cvSelected.setAlpha(0.5f);
        }
    }
}