package interactive.cide.com.kuriocompagnon;


import android.net.nsd.NsdServiceInfo;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class MessageManager {

    public static final String BATTERY = "BatteryInfo";
    public static final String TIMESLOT = "TimeSlotSetting";
    public static final String APPINFO = "AppInfo";


    private static final String GET="get";
    public static final String ICON = "Icon";

    private static final String TAG = "MessageManager";

    private final CompanionSocketClient companionSocketClient;


    public MessageManager(CompanionSocketClient companionSocketClient) {
        this.companionSocketClient = companionSocketClient;
    }

    public Boolean initializeManager(NsdServiceInfo serviceInfo){
        return companionSocketClient.createSocket(serviceInfo.getHost(), serviceInfo.getPort());
    }


    public void sendMessage(String msg) {
        try {


            Socket socket = companionSocketClient.getSocket();

            if (socket == null) {
                Log.d(TAG, "Socket is null, wtf?");
            } else if (socket.getOutputStream() == null) {
                Log.d(TAG, "Socket output stream is null, wtf?");
            }

            PrintWriter out = new PrintWriter(
                    new BufferedWriter(
                            new OutputStreamWriter(socket.getOutputStream())), true);
            out.println(msg);
            out.flush();

        } catch (UnknownHostException e) {
            Log.d(TAG, "Unknown Host", e);
        } catch (IOException e) {
            Log.d(TAG, "I/O Exception", e);
        } catch (Exception e) {
            Log.d(TAG, "Error3", e);
        }
        Log.d(TAG, "Client sent message: " + msg);
    }

    public void requestObject(final String action){

        new Thread() {
            @Override
            public void run() {
                sendMessage(GET+action);
            }
        }.start();

    }

    public void requestAssociation(final NsdServiceInfo item){

        new Thread() {
            @Override
            public void run() {
                if (initializeManager(item)){
                    sendMessage("association");
                }
            }
        }.start();

    }

    public void requestIcon(final String packageName){

        new Thread() {
            @Override
            public void run() {
                sendMessage(GET+ICON+"@"+packageName);
            }
        }.start();

    }




}
