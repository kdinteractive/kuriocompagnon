package interactive.cide.com.kuriocompagnon.ui;

import android.graphics.drawable.Drawable;

public class ChildProfile {

    private String name;
    private String gender;
    private Drawable avatar;

    public ChildProfile(String name, String gender, Drawable avatar) {
        this.name = name;
        this.gender = gender;
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Drawable getAvatar() {
        return avatar;
    }

    public void setAvatar(Drawable avatar) {
        this.avatar = avatar;
    }
}
