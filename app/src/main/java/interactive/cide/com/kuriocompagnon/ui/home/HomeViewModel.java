package interactive.cide.com.kuriocompagnon.ui.home;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.app.Application;
import android.content.SharedPreferences;
import android.widget.Switch;

import java.util.List;

public class HomeViewModel extends AndroidViewModel {

    private MutableLiveData<List<Switch>> switchValue;

    public HomeViewModel(@NonNull Application application, List<Switch> switches) {
        super(application);
        switchValue = new MutableLiveData<>();
        switchValue.setValue(switches);
    }

    public void setSwitchValue(List<Switch> mSwitch) {
        switchValue.setValue(mSwitch);
    }

    public LiveData<List<Switch>> getSwitchValue() {
        return switchValue;
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application mApplication;

        private List<Switch> mSwitchValue;


        public Factory(@NonNull Application application, List<Switch> switches) {
            mApplication = application;
            mSwitchValue = switches;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new HomeViewModel(mApplication, mSwitchValue);
        }
    }
}